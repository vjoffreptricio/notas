import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.io.BufferedReader;
import java.io.FileReader;

public class Notas {

    public static void main(String[] args) {
        final String nombreDeArchivo = "./jpverdezoto_ProgAlg_O20F21_[APEB2-20]_FilesInput.csv";
        
        crearArchivoCsv(nombreDeArchivo);
        procesarNotas();
        estadisticasNotas();
    }

    private static void crearArchivoCsv(String nombreDeArchivo) {
        crearNotas(nombreDeArchivo, ";");
    }

    private static String verificarNota(String nota) {
        String notaR = "";
        if (nota.equals("--")) {
            notaR = "0.00";
        } else {
            notaR = nota;
        }
        return notaR;
    }
    
    private static String randomNota(int valorMaximo) {
        double nota = (double) (new Random().nextInt(valorMaximo)) / 100;
        if (nota != 0.0) {
            String notaResultado = String.valueOf(nota);
            return notaResultado;
        } else {
            return "--";
        }
    }

    private static void crearNotas(String file, String delim) {
        final String NEXT_LINE = "\n";
        try {
            FileWriter fw = new FileWriter(file);

            fw.append("ESTUDIANTE").append(delim);
            fw.append("ACDB1 (0-3.50)").append(delim);
            fw.append("APEB1 (0-3)").append(delim);
            fw.append("AAB01 (0-1)").append(delim);
            fw.append("AAEPRB1 (0-2.5)").append(delim);
            fw.append("AAB1 (0-3.50)").append(delim);
            fw.append("BIM1 (0-10)").append(delim);

            fw.append("ACDB2 (0-3.50)").append(delim);
            fw.append("APEB2 (0-3)").append(delim);
            fw.append("AAB02 (0-1)").append(delim);
            fw.append("AAEPRB2 (0-2.5)").append(delim);
            fw.append("AAB2 (0-3.50)").append(delim);
            fw.append("BIM2 (0-10)").append(delim);

            fw.append("TOTALFIN (0-10)").append(delim);
            fw.append("ACU65 (0-6.5)").append(delim);
            fw.append("RECU (0-3.5)").append(delim);
            fw.append("FINAL (0-10)").append(delim);
            fw.append("ESTADO").append(NEXT_LINE);

            Random n = new Random();
            int numeroEstudiantes = n.nextInt(200) + 1;

            for (int i = 1; i <= numeroEstudiantes; i++) {
                fw.append("ESTUDIANTE" + i).append(delim);
                fw.append(randomNota(350)).append(delim);
                fw.append(randomNota(300)).append(delim);
                fw.append(randomNota(100)).append(delim);
                fw.append(randomNota(250)).append(delim);
                fw.append("").append(delim);
                fw.append("").append(delim);

                fw.append(randomNota(350)).append(delim);
                fw.append(randomNota(300)).append(delim);
                fw.append(randomNota(100)).append(delim);
                fw.append(randomNota(250)).append(delim);
                fw.append("").append(delim);
                fw.append("").append(delim);

                fw.append("").append(delim);
                fw.append("").append(delim);
                fw.append("").append(delim);
                fw.append("").append(delim);
                fw.append("").append(NEXT_LINE);
            }
            
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
    }
    
    private static void procesarNotas() {

        final String delim = ";";
        final String NEXT_LINE = "\n";
        try {
            FileWriter fw = new FileWriter("./jpverdezoto_ProgAlg_O20F21_[APEB2-20]_FilesOutput.csv");

            fw.append("ESTUDIANTE").append(delim);
            fw.append("ACDB1 (0-3.50)").append(delim);
            fw.append("APEB1 (0-3)").append(delim);
            fw.append("AAB01 (0-1)").append(delim);
            fw.append("AAEPRB1 (0-2.5)").append(delim);
            fw.append("AAB1 (0-3.50)").append(delim);
            fw.append("BIM1 (0-10)").append(delim);

            fw.append("ACDB2 (0-3.50)").append(delim);
            fw.append("APEB2 (0-3)").append(delim);
            fw.append("AAB02 (0-1)").append(delim);
            fw.append("AAEPRB2 (0-2.5)").append(delim);
            fw.append("AAB2 (0-3.50)").append(delim);
            fw.append("BIM2 (0-10)").append(delim);

            fw.append("TOTALFIN (0-10)").append(delim);
            fw.append("ACU65 (0-6.5)").append(delim);
            fw.append("RECU (0-3.5)").append(delim);
            fw.append("FINAL (0-10)").append(delim);
            fw.append("ESTADO").append(NEXT_LINE);

            BufferedReader bufferLectura = null;
            try {
                bufferLectura = new BufferedReader(new FileReader("./jpverdezoto_ProgAlg_O20F21_[APEB2-20]_FilesInput.csv"));

                String linea = bufferLectura.readLine();
                linea = bufferLectura.readLine();
                while (linea != null) {

                    String[] campos = linea.split(";");
					
					double AAB1 = Double.parseDouble(verificarNota(campos[3])) + Double.parseDouble(verificarNota(campos[4]));
					double BIM1 = Double.parseDouble(verificarNota(campos[1])) + Double.parseDouble(verificarNota(campos[2])) + AAB1;
                    
					double AAB2 = Double.parseDouble(verificarNota(campos[9])) + Double.parseDouble(verificarNota(campos[10]));
                    double BIM2 = Double.parseDouble(verificarNota(campos[7])) + Double.parseDouble(verificarNota(campos[8])) + AAB2;                     
					
					double TOTALFIN = (BIM1 + BIM2) / 2;
                    double ACU65 = (Double.parseDouble(verificarNota(campos[1])) + Double.parseDouble(verificarNota(campos[2])) + Double.parseDouble(verificarNota(campos[7])) + Double.parseDouble(verificarNota(campos[8]))) / 2;

                    double RECU, FINAL;
                    String nRECU;
                    if (TOTALFIN < 7) {
                        String notaP = randomNota(350);
                        if ( notaP.equals("--")) {
                            RECU = 0;
                            FINAL = TOTALFIN;
						} else {
                            RECU = Double.parseDouble(notaP);
                            FINAL = RECU + ACU65;
                        }
                    } else {
                        RECU = 0;
                        FINAL = TOTALFIN;
                    }

                    if (RECU == 0) {
                        nRECU = "";
                    } else {
                        nRECU = String.valueOf(RECU);
                    }

                    String ESTADO = "";
                    if (FINAL < 7) {
                        ESTADO = "No acreditado";
                    } else {
                        ESTADO = "Acreditado";
                    }

                    fw.append(campos[0]).append(delim);
                    fw.append(campos[1]).append(delim);
                    fw.append(campos[2]).append(delim);
                    fw.append(campos[3]).append(delim);
                    fw.append(campos[4]).append(delim);
                    fw.append((String.format("%.2f", AAB1)).replaceAll(",", ".")).append(delim);
                    fw.append((String.format("%.2f", BIM1)).replaceAll(",", ".")).append(delim);

                    fw.append(campos[7]).append(delim);
                    fw.append(campos[8]).append(delim);
                    fw.append(campos[9]).append(delim);
                    fw.append(campos[10]).append(delim);
                    fw.append((String.format("%.2f", AAB2)).replaceAll(",", ".")).append(delim);
                    fw.append((String.format("%.2f", BIM2)).replaceAll(",", ".")).append(delim);

                    fw.append((String.format("%.1f", TOTALFIN)).replaceAll(",", ".")).append(delim);
                    fw.append((String.format("%.2f", ACU65)).replaceAll(",", ".")).append(delim);
                    fw.append(nRECU).append(delim);
                    fw.append((String.format("%.1f", FINAL)).replaceAll(",", ".")).append(delim);
                    fw.append(ESTADO).append(NEXT_LINE);

                    linea = bufferLectura.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bufferLectura != null) {
                    try {
                        bufferLectura.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }

    }
    
    private static void estadisticasNotas() {

        BufferedReader bufferLectura = null,bufferLectura2 = null,bufferLectura3 = null,bufferLectura4 = null;
        try {
            bufferLectura = new BufferedReader(new FileReader("./jpverdezoto_ProgAlg_O20F21_[APEB2-20]_FilesOutput.csv"));
            bufferLectura2 = new BufferedReader(new FileReader("./jpverdezoto_ProgAlg_O20F21_[APEB2-20]_FilesOutput.csv"));
            bufferLectura3 = new BufferedReader(new FileReader("./jpverdezoto_ProgAlg_O20F21_[APEB2-20]_FilesOutput.csv"));
            bufferLectura4 = new BufferedReader(new FileReader("./jpverdezoto_ProgAlg_O20F21_[APEB2-20]_FilesOutput.csv"));
            String linea = bufferLectura.readLine();
            String linea2 = bufferLectura2.readLine();
            String linea3 = bufferLectura3.readLine();
            String linea4 = bufferLectura4.readLine();
            linea = bufferLectura.readLine();
            linea2 = bufferLectura2.readLine();
            linea3 = bufferLectura3.readLine();
            linea4 = bufferLectura4.readLine();

            double porcentaje11 = 0, porcentaje12 = 0, porcentaje13 = 0, estudiantes = 0;
            double porcentaje21 = 0, porcentaje22 = 0;
            double porcentaje31 = 0, porcentaje32 = 0;
            double porcentaje41 = 0;

            while (linea != null) {
                estudiantes += 1;
                String[] campos = linea.split(";");

                if (campos[4].isEmpty()) {
                    porcentaje11 = porcentaje11 + 1;
                }
                if (campos[10].isEmpty()) {
                    porcentaje12 = porcentaje12 + 1;
                }
                if (campos[15].isEmpty()) {
                    porcentaje13 = porcentaje13 + 1;
                }

                if (campos[1].isEmpty()) {
                    porcentaje21 = porcentaje21 + 1;
                }
                if (campos[7].isEmpty()) {
                    porcentaje22 = porcentaje22 + 1;
                }

                if ("Acreditado".equals(campos[17])) {
                    porcentaje31 = porcentaje31 + 1;
                } else {
                    porcentaje32 = porcentaje32 + 1;
                }

                porcentaje41 += Double.parseDouble(campos[16]);

                linea = bufferLectura.readLine();
            }

            System.out.println("ESTUDIANTES: " + String.format("%.0f", (estudiantes)));
            System.out.println("**************************************");
            System.out.println("Porcentaje de estudiantes que no se han presentado a rendir sus examenes presenciales: ");
            System.out.println("AAEPRB1: " + String.format("%.2f", ((porcentaje11 * 100) / estudiantes)) + "%");
            System.out.println("AAEPRB2: " + String.format("%.2f", ((porcentaje12 * 100) / estudiantes)) + "%");
            System.out.println("RECU: " + String.format("%.2f", ((porcentaje13 * 100) / estudiantes)) + "%");
            System.out.println("**************************************");
            System.out.println("Porcentaje de estudiantes que no evidencian participacion en cualquier de los siguientes componentes: ");
            System.out.println("ACDB1: " + String.format("%.2f", ((porcentaje21 * 100) / estudiantes)) + "%");
            System.out.println("ACDB2: " + String.format("%.2f", ((porcentaje22 * 100) / estudiantes)) + "%");
            System.out.println("**************************************");
            System.out.println("Porcentaje de estudiantes que han Aprobado y Reprobado: ");
            System.out.println("Aprobado: " + String.format("%.2f", ((porcentaje31 * 100) / estudiantes)) + "%");
            System.out.println("Reprobado: " + String.format("%.2f", ((porcentaje32 * 100) / estudiantes)) + "%");
            System.out.println("**************************************");
            System.out.println("Listado de estudiantes que superar el promedio " + String.format("%.1f", (porcentaje41 / estudiantes)) + " de la clase: ");
            while (linea2 != null) {
                String[] campos2 = linea2.split(";");
                if (Double.parseDouble(campos2[16]) >= (porcentaje41 / estudiantes)) {
                    System.out.println(campos2[0]+ " - Nota Final: " +campos2[16]);
                }
                linea2 = bufferLectura2.readLine();
            }
            System.out.println("**************************************");
            System.out.println("Listado de estudiantes que mas se acercan a la nota maxima de 10 pts: ");
            while (linea3 != null) {
                String[] campos3 = linea3.split(";");
                if (Double.parseDouble(campos3[16]) >= 8) {
                    System.out.println(campos3[0]+ " - Nota Final: " +campos3[16]);
                }
                linea3 = bufferLectura3.readLine();
            }
            System.out.println("**************************************");
            System.out.println("Listado de estudiantes que mas bajos de 4 estan en notas: ");
            while (linea4 != null) {
                String[] campos4 = linea4.split(";");
                if (Double.parseDouble(campos4[16]) < 4) {
                    System.out.println(campos4[0]+ " - Nota Final: " +campos4[16]);
                }
                linea4 = bufferLectura4.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferLectura != null) {
                try {
                    bufferLectura.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
